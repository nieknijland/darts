const notPossibleFinishes = [159, 162, 163, 165, 166, 168, 169];

export function isPossibleFinish(score) {
    if(score > 170 || notPossibleFinishes.includes(score)) {
        return false;
    }

    return true;
}