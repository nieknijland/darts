import React, { Component } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import GameSetup from './components/GameSetup';
import Game from './components/Game';
import NotFound from './components/NotFound';

class App extends Component {
    render() {
        return (
            <BrowserRouter basename="/darts">
                <Switch>
                    <Route exact={true} path="/" component={GameSetup} />
                    <Route path="/game/:matchKind" component={Game} />
                    <Route component={NotFound} />
                </Switch>
            </BrowserRouter>
        );
    }
}

export default App;
