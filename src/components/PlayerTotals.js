import React from 'react';

class PlayerTotals extends React.Component {

    render() {
        let playersListItems = this.props.players.map((player, index) => {
            return (
                <li key={`${index}_${player.name}`} className={`player-totals__list-item ${this.props.playerTurn === index && "player-totals__list-item--active"}`}>
                    <span>{player.name}</span>
                    <span>
                        {player.scores ? player.scores.reduce((previous, current) => previous - current, this.props.gameFrom) : this.props.gameFrom}
                    </span>
                </li>
            )
        });

        return (
            <section className="player-totals">
                <div className="player-totals__wrapper">
                    <ul className="player-totals__list">
                        {playersListItems}
                    </ul>
                </div>
            </section>
        )
    }
}

export default PlayerTotals;
