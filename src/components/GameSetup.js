import React from 'react';

class GameSetup extends React.Component {
    constructor() {
        super();

        this.playDarts = this.playDarts.bind(this);
    }

    playDarts(e) {
        e.preventDefault();

        const gameFrom = this.countInput.value;
        this.props.history.push(`/game/${gameFrom}`)
    }

    render() {
        return (
            <div className="game-setup-holder">
                <div className="game-setup__form">
                    <form onSubmit={this.playDarts}>
                        <h2>Set up your game:</h2>
                        <input type="number" required placeholder="301, 501, etc" defaultValue='501' ref={(input) => {this.countInput = input}} />
                        <div className="button-holder">
                            <button className="score-button btn--green" type="submit">Play darts!</button>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

export default GameSetup;
