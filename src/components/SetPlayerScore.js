import React from 'react';

import { isPossibleFinish } from '../lib/finishers';

class SetPlayerScore extends React.Component {
    constructor() {
        super();

        this.setScore = this.setScore.bind(this);
    }

    componentDidMount() {
        this.score.focus();
    }

    setScore(e) {
        e.preventDefault();

        const score = parseInt(this.score.value, 10);

        if(!(score >= 0 && score <= 180)) {
            alert("Your score must be between 0 and 180.");

            return;
        }

        const player = {...this.props.player};
        const game = {...this.props.game};

        // Set score
        if(!player.scores) {
            player.scores = [];
        }

        const scoreLeft = player.scores.reduce((previous, current) => {
            return previous - current;
        }, game.from);

        if(score < scoreLeft && scoreLeft - score !== 1) {
            player.scores.push(score);

            this.props.setScore(player);
            this.props.handleTurns();

            this.score.value = "";
        } else if(score === scoreLeft) {
            if(isPossibleFinish(score)) {
                player.scores.push(score);

                this.props.setScore(player);
                this.props.gameEnd();

                this.score.value = ""
            } else {
                alert(`${score} is not a possible finish.`);
            }
        } else {
            alert(`You have to throw ${scoreLeft} to win.`);
        }

        this.score.focus();
    }

    undoScore() {
        this.props.undoLastScore();

        this.score.focus();
    }

    render() {
        return (
            <div className="set-player-score">
                <h1 className="game-score-start">{this.props.gameFrom}</h1>
                <form className="score-form" onSubmit={(e) => this.setScore(e)}>
                    <input ref={(input) => this.score = input} className="score-input" type="number" placeholder="Your score" min="0" max="180" required/>
                    <button className="score-button" type="button" onClick={() => this.undoScore()}>Undo</button>
                    <button className="score-button btn--green" type="submit">Add Score</button>
                </form>
            </div>
        )
    }
}

export default SetPlayerScore;
