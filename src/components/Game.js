import React from 'react';
import PropTypes from 'prop-types';

import SetPlayerScore from './SetPlayerScore';
import PlayerScore from './PlayerScore';
import PlayerTotals from './PlayerTotals';

class Game extends React.Component {
    constructor() {
        super();

        this.startGame = this.startGame.bind(this);
        this.undoLastScore = this.undoLastScore.bind(this);
        this.setScore = this.setScore.bind(this);
        this.handleTurns = this.handleTurns.bind(this);
        this.gameEnd = this.gameEnd.bind(this);

        this.state = {
            players: [],
            game: {}
        };
    }

    componentWillMount() {
        this.startGame(this.props.match.params.matchKind);
    }

    startGame(from) {
        const game = {...this.state.game};
        let players = [...this.state.players];

        game.from = parseInt(from, 10);
        game.playerTurn = 0;
        game.turn = 0;
        game.end = false;

        // Hardcoded for now
        players = [
            {
                name: "Player 1"
            },
            {
                name: "Player 2"
            }
        ]

        this.setState({
            game: game,
            players: players
        });
    }

    setScore(player) {
        let players = [...this.state.players];

        players[this.state.game.playerTurn] = player;

        this.setState({
            players: players
        });
    }

    handleTurns() {
        const game = {...this.state.game};

        // Handle turns
        if(game.playerTurn < this.state.players.length - 1) {
            game.playerTurn++;
        } else {
            game.playerTurn = 0
            game.turn++;
        }

        this.setState({
            game: game
        });
    }

    gameEnd() {
        const game = {...this.state.game};

        game.end = true;

        this.setState({
            game: game
        });
    }

    undoLastScore() {
        const players = [...this.state.players];
        const playerCount = players.length;
        const game = {...this.state.game};

        if(!(game.turn === 0 && game.playerTurn === 0)) {
            const lastPlayerTurn = game.playerTurn - 1 < 0 ? playerCount - 1 : game.playerTurn - 1;

            // Remove the last score from the previous player
            players[lastPlayerTurn].scores.pop();

            // Set playerTurn en turn
            game.playerTurn = lastPlayerTurn;
            lastPlayerTurn === playerCount - 1 && game.turn--;

            this.setState({
                players: players,
                game: game
            });
        } else {
            alert("No previous scores available");
        }
    }

    render() {
        const gameEnded = this.state.game.end;

        const players = this.state.players;
        const playerTurn = this.state.game.playerTurn;
        const gameFrom = this.state.game.from;

        return (
            <div className="game-holder">
                <div className="set-player-score-holder">
                    {!gameEnded ? (
                        <SetPlayerScore gameFrom={gameFrom} setScore={this.setScore} undoLastScore={this.undoLastScore} game={this.state.game} player={players[playerTurn]} gameEnd={this.gameEnd} handleTurns={this.handleTurns} />
                    ) : (
                        <h1>Game ends</h1>
                    )}
                </div>
                <PlayerScore gameFrom={gameFrom} playerTurn={playerTurn} players={players} />
                <PlayerTotals players={players} playerTurn={playerTurn} gameFrom={gameFrom} />
            </div>
        )
    }
}

Game.propTypes = {
    match: PropTypes.object.isRequired
}

export default Game;
