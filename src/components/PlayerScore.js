import React from 'react';

class PlayerScore extends React.Component {

    constructor() {
        super();

        this.updateFontSize = this.updateFontSize.bind(this);

        this.state = {
            listItemHeight: 550,
            listItemFontSize: 36,
            listItemMaxHeight: 80
        }
    }

    componentDidUpdate() {
        this.props.players[this.props.playerTurn].scores && this.updateFontSize();
    }

    updateFontSize() {
        const oldListItemHeight = this.state.listItemHeight;

        const listHeight = this.list.clientHeight;
        const scoreCount = this.props.players[this.props.playerTurn].scores.length;

        const listItemHeight = listHeight / scoreCount;
        const listItemFontSize = listItemHeight / 1.4;

        if(oldListItemHeight !== listItemHeight) {
            this.setState({
                listItemHeight: listItemHeight,
                listItemFontSize: listItemFontSize >= 36 ? this.state.listItemFontSize : listItemFontSize,
                listItemMaxHeight: listItemHeight <= 80 ? listItemHeight : this.state.listItemMaxHeight
            });
        }
    }

    render() {
        const currentPlayer = this.props.players[this.props.playerTurn];
        let scoreListItems = [];

        currentPlayer.scores &&
        currentPlayer.scores.reduce((previous, current, index) => {
            const scoreLeft = previous - current;

            scoreListItems.push(
                <li key={`${currentPlayer.name}_${index}`} className="player-score__list-item" style={{fontSize: this.state.listItemFontSize + "px", maxHeight: this.state.listItemMaxHeight + "px"}}>
                    <span className="player-score__score-left">{scoreLeft}</span>
                    <span className="player-score__score">{current}</span>
                </li>
            )

            return scoreLeft;
        }, this.props.gameFrom)

        return (
            <section className="player-score">
                <div className="player-score__wrapper">
                    <h1 className="player-score__name">{currentPlayer.name}</h1>
                    <div className="player-score__vertical-divider"></div>
                    <div className="player-score__list-container">
                        <ul ref={(list) => this.list = list} className="player-score__list">
                            <li className="player-score__list-item" style={{fontSize: this.state.listItemFontSize + "px", maxHeight: this.state.listItemMaxHeight + "px"}}>
                                <span className="player-score__score-left">{this.props.gameFrom}</span>
                            </li>
                            {scoreListItems}
                        </ul>
                    </div>
                </div>
            </section>
        )
    }
}

export default PlayerScore;
